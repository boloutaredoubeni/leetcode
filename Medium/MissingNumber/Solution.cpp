#include "Solution.h"
#include <vector>

int Solution::missingNumber( std::vector<int>& nums ) {
    std::vector<int>::const_iterator it = nums.begin();

    while ( ++it != nums.end() ) {
        if ( *it - *(it-1) != 1 ) return *(it-1) + 1;
    }
}
